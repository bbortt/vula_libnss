VERSION := $(shell python3 setup.py version|tail -n1)
ARCH := $(shell uname -m)

.PHONY: test
test:
	echo ${VERSION}
	echo ${ARCH}

.PHONY: pypi-build
pypi-build:
	python3 -m build

.PHONY: pypi-upload
pypi-upload:
	# The following is because pypi blocks things unless they are built with a Docker SPOF
	# so we pretend to be manylinux but only expect to support current Debian and Ubuntu systems
	mv dist/vula_libnss-$(VERSION)-*.whl dist/vula_libnss-$(VERSION)-cp38-cp38-manylinux2014_$(ARCH).whl
	python3 -m twine upload --repository pypi dist/*$(VERSION)*

.PHONY: deb
deb:
	echo $(VERSION)
	python3 setup.py --command-packages=stdeb.command sdist_dsc
	cp -v misc/python3-vula-libnss.postinst deb_dist/vula-libnss-$(VERSION)/debian/
	cd deb_dist/vula-libnss-$(VERSION) &&	dpkg-buildpackage -rfakeroot -us -uc

.PHONY: clean
clean:
	-rm -rf build/ dist/ vula_libnss.egg-info deb_dist
	-cd nss-altfiles && make clean

